#!/usr/bin/env bash

app_urls="https://github.com/owncloud/mail/releases/download/v0.4.1/mail.tar.gz"
app_urls_z="$app_urls_z https://github.com/owncloudarchive/calendar/releases/download/v0.8.2/calendar.zip"
app_urls="$app_urls https://github.com/owncloudarchive/contacts/releases/download/v0.5.0.0/contacts.tar.gz"
#app_urls_z="https://apps.owncloud.com/CONTENT/content-files/165528-storagecharts2.zip"
app_urls_z="$app_urls_z https://github.com/owncloud/music/releases/download/v0.3.10/music.zip"
app_names="user_external mail calendar contacts music"


sudo -u www-data php /var/www/owncloud/index.php > /dev/null
sudo -u www-data php5 /var/www/owncloud/index.php > /dev/null

if [ -f /var/www/owncloud/data/.isSetup ]; then
    exit 0
fi

rm -Rf /var/www/owncloud/data/*
cp -ar /owncloud_bootstrap/. /var/www/owncloud/data/
touch /var/www/owncloud/data/.isSetup

sudo -u www-data php /var/www/owncloud/index.php > /dev/null
sudo -u www-data php5 /var/www/owncloud/index.php > /dev/null

cd /var/www/owncloud/apps
for url in $app_urls; do
    wget -O - $url | tar -xz
done
for url in $app_urls_z; do
    wget -O tmp.zip $url
    unzip -u tmp.zip
    rm tmp.zip
done

cd ..
for app in $app_names; do
    sudo -u www-data php5 /var/www/owncloud/occ app:enable $app
    sudo -u www-data php /var/www/owncloud/occ app:enable $app
done

sudo -u www-data php /var/www/owncloud/index.php > /dev/null
sudo -u www-data php5 /var/www/owncloud/index.php > /dev/null